<?php
    require "classes/Mysql.php";
    require "includes/header.php";
?>

    <div id="container" class="container_12">
        <?php include "includes/header1.php"; ?>
        <section id="resultsHeader">
            <h2>Search Results</h2><br />
            <h6>(Click on the customer ID to view/edit)</h6><br />
        </section><!-- end resultsHeader section -->
        <section class="resultsDetail">
            <?php
                $searchString = $_POST['searchCriteria'];
                $mysql = New Mysql();
                $results = $mysql->searchCustomers($searchString);

                if ($results) {
                    echo '<table><tr><th>ID</th><th>Customer Number</th><th>Contact Name</th><th>Company Name</th><th>Work Phone</th></tr>';
                    while ($row = $results->fetch_object()) {
                        $ID = $row->id;
                        if ($row->custNum == "0") {
                            $custNum = "No Customer Number listed in database";
                        } else {
                            $custNum = $row->custNum;
                        }
                        $firstName = $row->FirstName;
                        $lastName = $row->LastName;
                        $companyName = $row->CompanyName;
                        if ($row->WorkPhone == "") {
                            $workPhone = "No Phone# listed in database";
                        } else {
                            $workPhone = $row->WorkPhone;
                        }

                        echo "<tr><td><a href='customerdataView.php?id=$ID'>" . $ID . '</a></td><td>' . $custNum . '</td><td>' . $firstName . ' ' . $lastName . '</td><td>' . $companyName
                            . '</td><td>' . $workPhone . '</td></tr>';
                    }
                    echo '</table>';
                }
            ?>
        </section><!-- end resultsDetail section -->
        <?php include "includes/footer.php"; ?>
    </div><!-- end container div -->
</body>

</html>
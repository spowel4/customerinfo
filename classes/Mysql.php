<?php

require_once 'includes/constants.php';

class Mysql {
    private $conn;

    function __construct() {
        $dbServer = DB_SERVER;
        $dbName = DB_NAME;
        $dbUser = DB_USER;
        $dbPWD = DB_PASSWORD;
        $errorFile = ERROR_FILE;
        //$dsn = DSN;
        try {
            $this->conn = new mysqli("$dbServer", "$dbUser", "$dbPWD", "$dbName") or die();
            //$this->conn = new PDO($dsn);
            //$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
        }
        catch (Exception $e) {
            echo "There has been a problem connecting to the database.\nPlease refer to $errorFile for the cause of the problem.\n\n";
            file_put_contents($errorFile, $e->getMessage(), FILE_APPEND);
        }
    }

    function searchCustomers($searchString) {
        try {
            $result = $this->conn->query("SELECT id, custNum, FirstName, LastName, CompanyName, WorkPhone FROM contacts WHERE custNum LIKE '%$searchString%' OR FirstName LIKE '%$searchString%' OR LastName LIKE '%$searchString%' OR CompanyName LIKE '%$searchString%' OR WorkPhone LIKE '%$searchString%'");
            return $result;
        }
        catch (Exception $e) {
            echo "There has been a problem retrieving records from the database.\nPlease refer to $errorFile for the cause of the problem.\n\n";
            file_put_contents($errorFile, $e->getMessage(), FILE_APPEND);
        }
    }

    function retrieveCustomerByID($searchString) {
        try {
            $result = $this->conn->query("SELECT * FROM contacts WHERE id= '$searchString'");
            return $result;
        }
        catch (Exception $e) {
            echo "There has been a problem retrieving records from the database.\nPlease refer to $errorFile for the cause of the problem.\n\n";
            file_put_contents($errorFile, $e->getMessage(), FILE_APPEND);
        }        
    }

    function deleteCustomerData($searchString) {
        echo "This is a test.  This is only a test.  If this were a real deletion, the record would have been deleted.";
    }

    function saveCustomerData($dataArray) {
//        foreach($dataArray as $name => $value) {
//            echo "In mysql function; $name . ': ' . $value . '<br />'";
//        }
//        var_dump($dataArray["id"]);
        try {
            $sqlText = "UPDATE contacts SET firstName = '" . $dataArray["contactFirstName"] . "', ";
            $sqlText .= "lastName = '" . $dataArray["contactLastName"] . "', address1 = '" . $dataArray["address"] . "', ";
            $sqlText .= "poBox = '" . $dataArray["poBox"] . "', city = '" . $dataArray["city"] . "', state = '" . $dataArray["state"] . "', ";
            $sqlText .= "zipCode = '" . $dataArray["zipcode"] . "', companyName = '" . $dataArray["customerName"] . "', custNum = '" . $dataArray["custNumber"] . "', workPhone = '" . $dataArray["businessPhone"] . "', mobilePhone = '" . $dataArray["mobilePhone"] . "', faxNumber = '" . $dataArray["faxNumber"] . "', emailAddress = '" . $dataArray["emailAddress"] . "', contractType = '" . $dataArray["contractType"] . "', salesman = '" . $dataArray["salesperson"] . "', tech = '" . $dataArray["primaryTech"] . "', secondaryTech = '" . $dataArray["backupTech"] . "', genCustNotes = '" . $dataArray["genCustNotes"] . "', drivingDirections = '" . $dataArray["drivingDirections"] . "', serverAdminPW = '" . $dataArray["adminPW"] . "', terminalServerAddress = '" . $dataArray["rdpIP"] . "', isp = '" . $dataArray["isp"] . "', ispDNS1 = '" . $dataArray["ispDNS1"] . "', ispDNS2 = '" . $dataArray["ispDNS2"] . "', pop3Server = '" . $dataArray["popServer"] . "', smtpServer = '" . $dataArray["smtpServer"] . "', emailUsername = '" . $dataArray["mailLogin"] . "', emailPW = '" . $dataArray["mailPW"] . "', ispNotes1 = '" . $dataArray["sysConfigNotes"] . "', internetDomainName = '" . $dataArray["internetDN"] . "', publicIPs = '" . $dataArray["publicIPs"] . "', routerType = '" . $dataArray["connRouterType"] . "', routerWanIP = '" . $dataArray["connRouterWanIP"] . "', routerLanIP = '" . $dataArray["connRouterLanIP"] . "', routerUsername = '" . $dataArray["connRouterUN"] . "', routerPassword = '" . $dataArray["connRouterPW"] . "', softwarePhysicalLocale = '" . $dataArray["softwareLocale"] . "', wiringClosetLocale = '" . $dataArray["wiringClosetLocale"] . "', numberOfUsers = '" . $dataArray["numberUsers"] . "', userName1 = '" . $dataArray["userName1"] . "', password1 = '" . $dataArray["password1"] . "', userName2 = '" . $dataArray["userName2"] . "', password2 = '" . $dataArray["password2"] . "', userName3 = '" . $dataArray["userName3"] . "', password3 = '" . $dataArray["password3"] . "', userName4 = '" . $dataArray["userName4"] . "', password4 = '" . $dataArray["password4"] . "', userName5 = '" . $dataArray["userName5"] . "', password5 = '" . $dataArray["password5"] . "', userName6 = '" . $dataArray["userName6"] . "', password6 = '" . $dataArray["password6"] . "', userName7 = '" . $dataArray["userName7"] . "', password7 = '" . $dataArray["password7"] . "', userName8 = '" . $dataArray["userName8"] . "', password8 = '" . $dataArray["password8"] . "', userName9 = '" . $dataArray["userName9"] . "', password9 = '" . $dataArray["password9"] . "', userName10 = '" . $dataArray["userName10"] . "', password10 = '" . $dataArray["password10"] . "', userName11 = '" . $dataArray["userName11"] . "', password11 = '" . $dataArray["password11"] . "', userName12 = '" . $dataArray["userName12"] . "', password12 = '" . $dataArray["password12"] . "', userName13 = '" . $dataArray["userName13"] . "', password13 = '" . $dataArray["password13"] . "', userName14 = '" . $dataArray["userName14"] . "', password14 = '" . $dataArray["password14"] . "', userName15 = '" . $dataArray["userName15"] . "', password15 = '" . $dataArray["password15"] . "', userName16 = '" . $dataArray["userName16"] . "', password16 = '" . $dataArray["password16"] . "', userName17 = '" . $dataArray["userName17"] . "', password17 = '" . $dataArray["password17"] . "', userName18 = '" . $dataArray["userName18"] . "', password18 = '" . $dataArray["password18"] . "', userName19 = '" . $dataArray["userName19"] . "', password19 = '" . $dataArray["password19"] . "', userName20 = '" . $dataArray["userName20"] . "', password20 = '" . $dataArray["password20"] . "'";
            $sqlText .= " WHERE ID = '" . $dataArray["id"] . "'";
            //echo "sqltext: " . $sqlText;
            $result = $this->conn->query($sqlText);
            echo 'The update was successful, click <a href="customerdataview.php">HERE</a> to view the updated record';
        }
        catch (Exception $e) {
            echo "There has been a problem saving changes to the database.\nPlease refer to $errorFile for the cause of the problem.\n\n";
            file_put_contents($errorFile, $e->getMessage(), FILE_APPEND);
        }
    }

    function createNewCustomer($dataArray) {
//        foreach($dataArray as $name => $value) {
//            echo "In mysql function; $name . ': ' . $value . '<br />'";
//        }
//        var_dump($dataArray["id"]);
        try {
            $sqlText = "INSERT INTO contacts (firstName, lastName, address1, poBox, city, state, zipCode, ";
            $sqlText .= "companyName, customerName, custNum, workPhone, mobilePhone, faxNumber, emailAddress, ";
            $sqlText .= "contractType, salesman, tech, secondaryTech, genCustNotes, drivingDirections, serverAdminPW, ";
            $sqlText .= "terminalServerAddress, rdpIP, isp, ispDNS1, ispDNS2, pop3Server, smtpServer, emailUsername, ";
            $sqlText .= "emailPW, ispNotes1, internetDomainName, publicIPs, routerType, routerWanIP, routerLanIP, ";
            $sqlText .= " routerUsername, routerPassword, softwarePhysicalLocale, wiringClosetLocale, numberOfUsers, ";
            $sqlText .= "userName1, password1, userName2, password2, userName3, password3, userName4, password4, ";
            $sqlText .= "userName5, password5, userName6, password6, userName7, password7, userName8, password8, ";
            $sqlText .= "userName9, password9, userName10, password10, userName11, password11, userName12, password12, ";
            $sqlText .= "userName13, password14, userName15, password15, userName16, password16, userName17, password17, ";
            $sqlText .= "userName18, password18, userName19, password19, userName20, password20) VALUES (";
            $sqlText .= "'". $dataArray["contactFirstName"] . "','" . $dataArray["contactLastName"] . "','" . $dataArray["address"] . "','";
            $sqlText .= ". $dataArray["poBox"] . "','" . $dataArray["city"] . "','" . $dataArray["state"] . "','";
            $sqlText .= ". $dataArray["zipcode"] . "','" . $dataArray["customerName"] . "','" . $dataArray["custNumber"] . "','";
            $sqlText .= ". $dataArray["businessPhone"] . "','" . $dataArray["mobilePhone"] . "','" . $dataArray["faxNumber"] . "','";
            $sqlText .= ". $dataArray["emailAddress"] . "','" . $dataArray["contractType"] . "','" . $dataArray["salesperson"] . "','";
            $sqlText .= ". $dataArray["primaryTech"] . "','" . $dataArray["backupTech"] . "','" . $dataArray["genCustNotes"] . "','";
            $sqlText .= ". $dataArray["drivingDirections"] . "','" . $dataArray["adminPW"] . "','" . $dataArray["rdpIP"] . "','";
            $sqlText .= ". $dataArray["isp"] . "','" . $dataArray["ispDNS1"] . "','" . $dataArray["ispDNS2"] . "','" . $dataArray["popServer"] . "','";
            $sqlText .= ". $dataArray["smtpServer"] . "','" .  $dataArray["mailLogin"] . "','" .  $dataArray["mailPW"] . "','";
            $sqlText .= ". $dataArray["sysConfigNotes"] . "','" . $dataArray["internetDN"] . "','" . $dataArray["publicIPs"] . "','";
            $sqlText .= ". $dataArray["connRouterType"] . "','" . $dataArray["connRouterWanIP"] . "','" . $dataArray["connRouterLanIP"] . "','";
            $sqlText .= ". $dataArray["connRouterUN"] . "','" . $dataArray["connRouterPW"] . "','" . $dataArray["softwareLocale"] . "','";
            $sqlText .= ". $dataArray["wiringClosetLocale"] . "','" . $dataArray["numberUsers"] . "','" . $dataArray["userName1"] . "','";
            $sqlText .= ". $dataArray["password1"] . "','" . $dataArray["userName2"] . "','" . $dataArray["password2"] . "','" . $dataArray["userName3"] . "','";
            $sqlText .= ". $dataArray["password3"] . "','" . $dataArray["userName4"] . "','" . $dataArray["password4"] . "','" . $dataArray["userName5"] . "','";
            $sqlText .= ". $dataArray["password5"] . "','" . $dataArray["userName6"] . "','" . $dataArray["password6"] . "','" . $dataArray["userName7"] . "','";
            $sqlText .= ". $dataArray["password7"] . "','" . $dataArray["userName8"] . "','" . $dataArray["password8"] . "','" . $dataArray["userName9"] . "','";
            $sqlText .= ". $dataArray["password9"] . "','" . $dataArray["userName10"] . "','" . $dataArray["password10"] . "','" . $dataArray["userName11"] . "','";
            $sqlText .= ". $dataArray["password11"] . "','" . $dataArray["userName12"] . "','" . $dataArray["password12"] . "','";
            $sqlText .= ". $dataArray["userName13"] . "','" . $dataArray["password13"] . "','" . $dataArray["userName14"] . "','";
            $sqlText .= ". $dataArray["password14"] . "','" . $dataArray["userName15"] . "','" . $dataArray["password15"] . "','";
            $sqlText .= ". $dataArray["userName16"] . "','" . $dataArray["password16"] . "','" . $dataArray["userName17"] . "','";
            $sqlText .= ". $dataArray["password17"] . "','" . $dataArray["userName18"] . "','" . $dataArray["password18"] . "','";
            $sqlText .= ". $dataArray["userName19"] . "','" . $dataArray["password19"] . "','" . $dataArray["userName20"] . "','";
            $sqlText .= ". $dataArray["password20"] . "')";
            $sqlText .= " WHERE ID = '" . $dataArray["id"] . "'";
            echo "sqltext: " . $sqlText;
            $result = $this->conn->query($sqlText);
            if ($result) {
                echo 'The update was successful, click <a href="customerdataview.php">HERE</a> to view the updated record';
            } else {
                throw new exception("The new customer creation failed");
            }

        }
        catch (Exception $e) {
            echo "There has been a problem saving changes to the database.\nPlease refer to $errorFile for the cause of the problem.\n\n";
            file_put_contents($errorFile, $e->getMessage(), FILE_APPEND);
        }
    }    
}
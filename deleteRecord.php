<?php
    require "classes/Mysql.php";
    require "includes/header.php"; ?>

<body>
    <?php include "includes/header1.php"; ?>
    <div id="container">
        <?php
            echo "deleteRecord.php<br />";
            if (isset($_POST)) {
                $searchString = $_POST['id'];
                //echo $searchString;
                $mysql = New Mysql();
                $result = $mysql->deleteCustomerData($searchString);
            } else {
                echo "Nothing to do!";
            }
        ?>
    </div>
    <?php include "includes/footer.php"; ?>
</body>

</html>
<?php

class CustomerInfoModel extends CI_Model {
	
	function getAll() {
		$q = $this->db->get('Contacts');
		
		if ($q->num_rows() > 0) {
			foreach ($q->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
	}

	function getCustomer($searchString) {
		//echo 'Hello from getCustomer'; 		//for debugging purposes only
		$sql = "SELECT Number, FirstName, LastName, CompanyName, WorkPhone FROM Contacts WHERE FirstName LIKE '%$searchString%' OR LastName LIKE '%$searchString%' OR CompanyName LIKE '%$searchString%' OR WorkPhone LIKE '%$searchString%'";
		$q = $this->db->query($sql);

		if ($q->num_rows() > 0) {
			foreach ($q->result() as $row) {
				$data[] = $row;
			}
			return $data;
		} else {
			$data = 'No Records Found';
			return $data;
		}
	}
}
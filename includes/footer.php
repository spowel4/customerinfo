<!-- <div class="footer">
    <img src="img/php-power-white.png" alt="powered by PHP" />
    <img src="img/powered-by-mysql.png" height="64" width="125" alt="powered by MySQL" />
    <img src="img/opensource-logo.png" height="64" width="125" alt="powered by Open Source Software" />
</div> -->

<footer id="pageFooter" class="grid_12"><!-- html5 tag for footer section -->
    <img src="img/php-power-white.png" alt="powered by PHP" class="grid_2 img_pageFooter"/>
    <img src="img/powered-by-mysql.png" alt="powered by MySQL" class="grid_2 img_pageFooter"/>
    <img src="img/opensource-logo_small.png" alt="powered by Open Source Software" class="grid_2 img_pageFooter"/>
</footer>
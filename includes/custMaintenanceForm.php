        <section id="maintenanceDetail" class="grid_12">
            <form id="customerMaintenance" action="" method="post">
                <section id="contactInfo">
                    <label for="customerName">Customer Name</label><input type="text" name="customerName" value="<?php echo $companyName; ?>" />
                    <label for="contactFirstName">Contact First Name</label><input type="text" name="contactFirstName" value="<?php echo $firstName; ?>" />
                    <label for="contactLastName">Contact Last Name</label><input type="text" name="contactLastName" value="<?php echo $lastName; ?>" />   
                    <label for="address">Address</label><textarea name="address"><?php echo $address; ?></textarea>
                    <label for="emailAddress">Email Address</label><input type="text" name="emailAddress" value="<?php echo $emailAddress; ?>" />
                    <label for="contactID">Contact ID</label><input type="text" name="contactID" value="<?php echo $contactID; ?>" />
                    <label for="numberUsers">Approx.# Users</label><input type="text" name="numberUsers" value="<?php echo $numberOfUsers; ?>" />
                    <label for="poBox">P.O. Box</label><input type="text" name="poBox" value="<?php echo $poBox; ?>" />   
                    <label for="city">City</label><input type="text" name="city" value="<?php echo $city; ?>" />      
                    <label for="state">State</label><input type="text" name="state" value="<?php echo $state; ?>" />  
                    <label for="zipcode">Zipcode</label><input type="text" name="zipcode" value="<?php echo $zipcode; ?>" />      
                    <label for="primaryTech">Primary Tech</label><input type="text" name="primaryTech" value="<?php echo $primaryTech; ?>" />
                    <label for="backupTech">Backup Tech</label><input type="text" name="backupTech" value="<?php echo $secondaryTech; ?>" />
                    <label for="salesperson">Salesperson</label><input type="text" name="salesperson" value="<?php echo $salesman; ?>" />
                    <label for="businessPhone">Business Phone</label><input type="text" name="businessPhone" value="<?php echo $workPhone; ?>" />
                    <label for="mobilePhone">Mobile Phone</label><input type="text" name="mobilePhone" value="<?php echo $mobilephone; ?>" /> 
                    <label for="faxNumber">Fax#</label><input type="text" name="faxNumber" value="<?php echo $faxNumber; ?>" />
                </section><!-- end contactInfo section -->
                <section id="notesAndLogins">
                    <section id="logins">
                        <label>UserName</label><label>Password</label>
                        <input type="text" name="userName1" value="<?php echo $userName1; ?>" /><input type="text" name="password1" value="<?php echo $password1; ?>" />
                        <input type="text" name="userName2" value="<?php echo $userName2; ?>" /><input type="text" name="password2" value="<?php echo $password2; ?>" />
                        <input type="text" name="userName3" value="<?php echo $userName3; ?>" /><input type="text" name="password3" value="<?php echo $password3; ?>" />
                        <input type="text" name="userName4" value="<?php echo $userName4; ?>" /><input type="text" name="password4" value="<?php echo $password4; ?>" />
                        <input type="text" name="userName5" value="<?php echo $userName5; ?>" /><input type="text" name="password5" value="<?php echo $password5; ?>" />
                        <input type="text" name="userName6" value="<?php echo $userName6; ?>" /><input type="text" name="password6" value="<?php echo $password6; ?>" />
                        <input type="text" name="userName7" value="<?php echo $userName7; ?>" /><input type="text" name="password7" value="<?php echo $password7; ?>" />
                        <input type="text" name="userName8" value="<?php echo $userName8; ?>" /><input type="text" name="password8" value="<?php echo $password8; ?>" />
                        <input type="text" name="userName9" value="<?php echo $userName9; ?>" /><input type="text" name="password9" value="<?php echo $password9; ?>" />
                        <input type="text" name="userName10" value="<?php echo $userName10; ?>" /><input type="text" name="password10" value="<?php echo $password10; ?>" />
                        <input type="text" name="userName11" value="<?php echo $userName11; ?>" /><input type="text" name="password11" value="<?php echo $password11; ?>" />
                        <input type="text" name="userName12" value="<?php echo $userName12; ?>" /><input type="text" name="password12" value="<?php echo $password12; ?>" />
                        <input type="text" name="userName13" value="<?php echo $userName13; ?>" /><input type="text" name="password13" value="<?php echo $password13; ?>" />
                        <input type="text" name="userName14" value="<?php echo $userName14; ?>" /><input type="text" name="password14" value="<?php echo $password14; ?>" />
                        <input type="text" name="userName15" value="<?php echo $userName15; ?>" /><input type="text" name="password15" value="<?php echo $password15; ?>" />
                        <input type="text" name="userName16" value="<?php echo $userName16; ?>" /><input type="text" name="password16" value="<?php echo $password16; ?>" />
                        <input type="text" name="userName17" value="<?php echo $userName17; ?>" /><input type="text" name="password17" value="<?php echo $password17; ?>" />
                        <input type="text" name="userName18" value="<?php echo $userName18; ?>" /><input type="text" name="password18" value="<?php echo $password18; ?>" />
                        <input type="text" name="userName19" value="<?php echo $userName19; ?>" /><input type="text" name="password19" value="<?php echo $password19; ?>" />
                        <input type="text" name="userName20" value="<?php echo $userName20; ?>" /><input type="text" name="password20" value="<?php echo $password20; ?>" />
                    </section><!-- end of logins section -->
                    <section id="notes">
                        <label for="drivingDirections">Driving Directions</label><textarea rows="10" cols="20" name="drivingDirections"><?php echo $drivingDirections; ?></textarea>
                        <label for="softwareLocale">Software Locale</label><input type="text" name="softwareLocale" value="<?php echo $softwarePhysicalLocale; ?>" />
                        <label for="wiringClosetLocale">Wiring Closet Locale</label><input type="text" name="wiringClosetLocale" value="<?php echo $wiringClosetLocale; ?>" />  
                        <label for="custNumber">Customer#</label><input type="text" id="custNumber" name="custNumber" value="<?php echo $custNum; ?>" />        
                        <label for="businessHours">Business Hours</label<input type="text" name="businessHours" value="<?php echo $officeHours; ?>" />  
                        <label for="contractNumber">Contract Number</label>
                        <select id="contractNumber" name="contractType">
                            <option value="R-NT">R-NT</option>
                            <option value="SW">SW</option>
                            <option value="CON">CON</option>
                        </select>       
                        <label for="genCustNotes">General Customer Notes</label><textarea rows="10" cols="20" name="genCustNotes"><?php echo $genCustNotes; ?></textarea>
                    </section><!-- end of notes section -->
                </section><!-- end of notesAndLogins section -->
                <section id="networkInfo">
                    <label for="isp">ISP</label><input type="text" name="isp" value="<?php echo $isp; ?>" />
                    <label for="ispDNS1">DNS1</label><input type="text" name="ispDNS1" value="<?php echo $ispDNS1; ?>" />
                    <label for="ispDNS2">DNS2</label><input type="text" name="ispDNS2" value="<?php echo $ispDNS2; ?>" />     
                    <label for="publicIPs">Public IPs</label><input type="text" name="publicIPs" value="<?php echo $publicIPs; ?>" /> 
                    <label for="adminPW">Admin PW</label><input type="text" name="adminPW" value="<?php echo $serverAdminPW; ?>" />
                    <label for="logmein">LogMeIn</label><input type="checkbox" name="logmein" />
                    <label for="rdpIP">Terminal Services IP</label><input type="text" name="rdpIP" value="<?php echo $terminalServerAddress; ?>" />       
                    <label for="internetDN">Internet Domain Name</label><input type="text" name="internetDN" value="<?php echo $internetDomainName; ?>" />    
                    <label value="Connection Router">Connection Router</label>
                    <label for="connRouterType">Type</label><input type="text" name="connRouterType" value="<?php echo $routerType; ?>" />
                    <label for="connRouterWanIP">WAN IP</label><input type="text" name="connRouterWanIP" value="<?php echo $routerWanIP; ?>" />   
                    <label for="connRouterLanIP">LAN IP</label><input type="text" name="connRouterLanIP" value="<?php echo $routerLanIP; ?>" />       
                    <label for="connRouterUN">Username</label><input type="text" name="connRouterUN" value="<?php echo $routerUsername; ?>" />    
                    <label for="connRouterPW">Password</label><input type="text" name="connRouterPW" value="<?php echo $routerPassword; ?>" />        
                    <label>Firewall Router</label>  
                    <label>Username</label>
                    <label>Password</label>
                    <label>Internet Email Setup</label>
                    <label for="directMail">Direct Mail</label><input type="checkbox" name="directMail" />
                    <label for="catchallMail">CatchAll Mail</label><input type="checkbox" name="catchallMail" value="<?php echo $catchAll; ?>" />         
                    <label for="ispSpoolingMail">ISP Spooling Mail</label><input type="checkbox" name="ispSpoolingMail" value="<?php echo $ispSpooling; ?>" />     
                    <label for="ispHostingMail">ISP Hosting Mail</label><input type="checkbox" name="ispHostingMail" />
                    <label for="spamService">Spam Service</label><input type="checkbox" name="spamService" /> 
                    <label for="popServer">POP Server</label><input type="text" name="popServer" value="<?php echo $pop3Server; ?>" />        
                    <label for="smtpServer">SMTP Server</label><input type="text" name="smtpServer" value="<?php echo $smtpServer; ?>" /> 
                    <label for="mailService">Service</label><input type="text" name="mailService" value="<?php echo $serviceName; ?>" />
                    <label for="mailLogin">Email Login</label><input type="text" name="mailLogin" value="<?php echo $emailUsername; ?>" /> 
                    <label for="mailPW">Email Password</label><input type="text" name="mailPW" value="<?php echo $emailPW; ?>" /> 
                    <label for="sysConfigNotes">System Config Notes</label><textarea rows="10" cols="20" name="sysConfigNotes"><?php echo $ispNotes1; ?></textarea>
                </section><!-- end of networkInfo section -->
                <section class="formButtons">
                    <input type="submit" id="extendedNotes" name="submission" value="Extended Notes" onclick="return this.form.action='extendedNotes.php'"/>
                    <input type="submit" id="southwareInfo" name="submission" value="Southware Info" onclick="return this.form.action='southwareinfo.php'"/>
                    <input type="submit" id="callNotes" name="submission" value="Call Notes" onclick="return this.form.action='callNotes.php'"/>
                </section><!-- end of formButtons section -->
                <section id="formActionButtons">
                    <input type="submit" id="saveChanges" name="submission" value="Save Changes" onclick="return this.form.action='saveChanges.php'"/>
                    <input type="submit" id="newRecord" name="submission" value="New" onclick="return this.form.action='newRecord.php'"/>
                    <input type="submit" id="cancel" name="submission" value="Cancel (Back to Search)" onclick="return this.form.action='customersearchView.php'"/>
                    <input type="submit" id="deleteRecord" name="submission" value="Delete" onclick="return this.form.action='deleteRecord.php'"/>
                    <input type="submit" id="printRecord" name="submission" value="Print" onclick="return this.form.action='printRecord.php'"/>
                </section><!-- end of formActionButtons section -->
            </form><!-- end of formMaintenance form -->
        </section><!-- end of maintenanceDetail section-->
        <section id="maintenanceDetail" class="grid_12">
            <form id="customerMaintenance" action="" method="post">
                <section id="contactInfo">
                    <label for="customerName">Customer Name</label><input type="text" name="customerName" value="" />
                    <label for="contactFirstName">Contact First Name</label><input type="text" name="contactFirstName" value="" />
                    <label for="contactLastName">Contact Last Name</label><input type="text" name="contactLastName" value="" />
                    <label for="address">Address</label><textarea name="address"></textarea>
                    <label for="emailAddress">Email Address</label><input type="text" name="emailAddress" value="" />
                    <label for="contactID">Contact ID</label><input type="text" name="contactID" value="" />
                    <label for="numberUsers">Approx.# Users</label><input type="text" name="numberUsers" value="" />
                    <label for="poBox">P.O. Box</label><input type="text" name="poBox" value="" />
                    <label for="city">City</label><input type="text" name="city" value="" />
                    <label for="state">State</label><input type="text" name="state" value="" />
                    <label for="zipcode">Zipcode</label><input type="text" name="zipcode" value="" />
                    <label for="primaryTech">Primary Tech</label><input type="text" name="primaryTech" value="" />
                    <label for="backupTech">Backup Tech</label><input type="text" name="backupTech" value="" />
                                        <label for="salesperson">Salesperson</label><input type="text" name="salesperson" value="" />
                    <label for="businessPhone">Business Phone</label><input type="text" name="businessPhone" value="" />
                    <label for="mobilePhone">Mobile Phone</label><input type="text" name="mobilePhone" value="" /> 
                    <label for="faxNumber">Fax#</label><input type="text" name="faxNumber" value="" />
                </section><!-- end contactInfo section -->
                <section id="notesAndLogins">
                    <section id="logins">
                        <label>UserName</label><label>Password</label>
                        <input type="text" name="userName1" value="" /><input type="text" name="password1" value="" />
                        <input type="text" name="userName2" value="" /><input type="text" name="password2" value="" />
                        <input type="text" name="userName3" value="" /><input type="text" name="password3" value="" />
                        <input type="text" name="userName4" value="" /><input type="text" name="password4" value="" />
                        <input type="text" name="userName5" value="" /><input type="text" name="password5" value="" />
                        <input type="text" name="userName6" value="" /><input type="text" name="password6" value="" />
                        <input type="text" name="userName7" value="" /><input type="text" name="password7" value="" />
                        <input type="text" name="userName8" value="" /><input type="text" name="password8" value="" />
                        <input type="text" name="userName9" value="" /><input type="text" name="password9" value="" />
                        <input type="text" name="userName10" value="" /><input type="text" name="password10" value="" />
                        <input type="text" name="userName11" value="" /><input type="text" name="password11" value="" />
                        <input type="text" name="userName12" value="" /><input type="text" name="password12" value="" />
                        <input type="text" name="userName13" value="" /><input type="text" name="password13" value="" />
                        <input type="text" name="userName14" value="" /><input type="text" name="password14" value="" />
                        <input type="text" name="userName15" value="" /><input type="text" name="password15" value="" />
                        <input type="text" name="userName16" value="" /><input type="text" name="password16" value="" />
                        <input type="text" name="userName17" value="" /><input type="text" name="password17" value="" />
                        <input type="text" name="userName18" value="" /><input type="text" name="password18" value="" />
                        <input type="text" name="userName19" value="" /><input type="text" name="password19" value="" />
                        <input type="text" name="userName20" value="" /><input type="text" name="password20" value="" />
                    </section><!-- end of logins section -->
                    <section id="notes">
                        <label for="drivingDirections">Driving Directions</label><textarea rows="10" cols="20" name="drivingDirections"></textarea>
                        <label for="softwareLocale">Software Locale</label><input type="text" name="softwareLocale" value="" />
                        <label for="wiringClosetLocale">Wiring Closet Locale</label><input type="text" name="wiringClosetLocale" value="" />
                        <label for="custNumber">Customer#</label><input type="text" id="custNumber" name="custNumber" value="" />
                        <label for="businessHours">Business Hours</label<input type="text" name="businessHours" value="" />
                        <label for="contractNumber">Contract Number</label>
                        <select id="contractNumber" name="contractType">
                            <option value="R-NT">R-NT</option>
                            <option value="SW">SW</option>
                            <option value="CON">CON</option>
                        </select>       
                        <label for="genCustNotes">General Customer Notes</label><textarea rows="10" cols="20" name="genCustNotes"></textarea>
                    </section><!-- end of notes section -->
                </section><!-- end of notesAndLogins section -->
                <section id="networkInfo">
                    <label for="isp">ISP</label><input type="text" name="isp" value="" />
                    <label for="ispDNS1">DNS1</label><input type="text" name="ispDNS1" value="" />
                    <label for="ispDNS2">DNS2</label><input type="text" name="ispDNS2" value="" />
                    <label for="publicIPs">Public IPs</label><input type="text" name="publicIPs" value="" />
                    <label for="adminPW">Admin PW</label><input type="text" name="adminPW" value="" />
                    <label for="logmein">LogMeIn</label><input type="checkbox" name="logmein" />
                    <label for="rdpIP">Terminal Services IP</label><input type="text" name="rdpIP" value="" />
                    <label for="internetDN">Internet Domain Name</label><input type="text" name="internetDN" value="" />
                    <label value="Connection Router">Connection Router</label>
                    <label for="connRouterType">Type</label><input type="text" name="connRouterType" value="" />
                    <label for="connRouterWanIP">WAN IP</label><input type="text" name="connRouterWanIP" value="" />
                    <label for="connRouterLanIP">LAN IP</label><input type="text" name="connRouterLanIP" value="" />
                    <label for="connRouterUN">Username</label><input type="text" name="connRouterUN" value="" />
                    <label for="connRouterPW">Password</label><input type="text" name="connRouterPW" value="" />
                    <label>Firewall Router</label>
                    <label>Username</label>
                    <label>Password</label>
                    <label>Internet Email Setup</label>
                    <label for="directMail">Direct Mail</label><input type="checkbox" name="directMail" />
                    <label for="catchallMail">CatchAll Mail</label><input type="checkbox" name="catchallMail" value="" />
                    <label for="ispSpoolingMail">ISP Spooling Mail</label><input type="checkbox" name="ispSpoolingMail" value="" />
                    <label for="ispHostingMail">ISP Hosting Mail</label><input type="checkbox" name="ispHostingMail" />
                    <label for="spamService">Spam Service</label><input type="checkbox" name="spamService" /> 
                    <label for="popServer">POP Server</label><input type="text" name="popServer" value="" />
                    <label for="smtpServer">SMTP Server</label><input type="text" name="smtpServer" value="" />
                    <label for="mailService">Service</label><input type="text" name="mailService" value="" />
                    <label for="mailLogin">Email Login</label><input type="text" name="mailLogin" value="" />
                    <label for="mailPW">Email Password</label><input type="text" name="mailPW" value="" />
                    <label for="sysConfigNotes">System Config Notes</label><textarea rows="10" cols="20" name="sysConfigNotes"></textarea>
                </section><!-- end of networkInfo section -->
                <section id="formActionButtons">
                    <input type="submit" id="newRecord" name="submission" value="Create New Record" onclick="return this.form.action=''"/>
                    <?php
                        $mysql = New Mysql();
                        $results = $mysql->createNewCustomer($_POST);
                    ?>
                    <input type="submit" id="cancel" name="submission" value="Cancel (Back to Search)" onclick="return this.form.action='customersearchView.php'"/>
                    <input type="submit" id="printRecord" name="submission" value="Print" onclick="return this.form.action='printRecord.php'"/>
                </section><!-- end of formActionButtons section -->
            </form><!-- end of formMaintenance form -->
        </section><!-- end of maintenanceDetail section-->
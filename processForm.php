<?php
    switch ($_POST['submission']) {
        case "Save Changes":
            header('Location: saveChanges.php');
            break;

        case "New":
            header('Location: newRecord.php');
            break;

        case "Cancel (Back to Search)":
            header('Location: customersearchView.php');
            break;

        case "Delete":
            header('Location: deleteRecord.php');
            break;

        case "Print":
            header('Location: printRecord.php');
            break;

        case "Extended Notes":
            header('Location: extendedNotes.php');
            break;

        case "SouthWare Info":
            header('Location: southwareInfo.php');
            break;

        case "Call Notes":
            header('Location: callNotes.php');
            break;

        default:
            header('Location: errors.php');
    }
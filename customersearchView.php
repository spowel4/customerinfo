<?php require "includes/header.php"; ?>

<body onload="document.searchCustomers.searchCriteria.focus()">
    <div id="container" class="container_12">
      <?php include "includes/header1.php"; ?>
      <div id="usageInstructions" class="grid_12">
          <h2>Customer Information Database:</h2><br />
          <p>Enter at least two characters from the Company Name or Contact
               Name or Phone Number and then click the Submit button
          </p>
          <br />
          <p>You can also search by the SouthWare customer number</p>
          <br />
      </div><!-- end usageInstructions div -->
      <div id="searchForm" class="grid_12">
          <form id="searchCustomers" name="searchCustomers" method="post" action="customerinfoView.php">
              <p>
                  <label for="searchCriteria">Enter Search Criteria:</label>
                  <input type="text" name="searchCriteria" class="textInput" />
              </p>
              <p><input type="submit", value="Submit" />
          </form><!-- end searchCustomers form -->
      </div><!-- end searchForm div -->
      <?php include "includes/footer.php"; ?>
    </div><!-- end container div -->
</body>

</html>

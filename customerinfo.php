<?php

class CustomerInfo extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	function index() {
		$this->load->model('customerinfoModel');
		$this->load->view('customerdataView');
	}

	function searchCustomers() {
		//echo 'Hello from searchCustomers'; die();	//for debugging purposes only
		$this->load->model('customerinfoModel');
		$this->load->library('form_validation');

		//parameters to set_rules function: field name, error message, validation rules
		$this->form_validation->set_rules('name', 'Name', 'trim|required');

		if ($this->form_validation->run() == FALSE) {		//validation has failed, reload the form
			$this->load->view('customersearchView');
		}
		else
		{		//validation has passed, now do the search
			$searchString = $this->input->post('name');
			//echo 'searchString: ', $searchString;	//for debugging purposes only
			$data['records'] = $this->customerinfoModel->getCustomer($searchString);
		}
		$this->load->view('customerinfoView', $data);
	}

	function modifyCustomers() {
		$this->load->view('customerdataView');
	}
}